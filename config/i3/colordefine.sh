#!/bin/bash

VAR=$(echo $USER | md5sum)

echo "#ifndef COLORDEFINE_H"
echo "#define COLORDEFINE_H"
echo
echo "#define USERCOLOR_R 0x${VAR:0:2}"
echo "#define USERCOLOR_G 0x${VAR:2:2}"
echo "#define USERCOLOR_B 0x${VAR:4:2}"
echo
echo "#endif // COLORDEFINE_H"


