
#include <cstdlib>
#include <iostream>

#include "confgenlib.h"

using namespace std;


int main() {

  map<string, Mode> modes;

  modes = confgen(
      {{"left"  , {{'t'}, {"left"}}}, // key
       {"down"  , {{'s'}, {"down"}}},
       {"up"    , {{'r'}, {"up"}}},
       {"right" , {{'n'}, {"right"}}},
       {"a" , {{'a'}}},
       {"b" , {{'k'}}},
       {"c" , {{'x'}}},
       {"d" , {{'i'}}},
       {"e" , {{'p'}}},
       {"f" , {{'e'}}},
       {"g" , {{"comma"}}}, // ,
       {"h" , {{'c'}}},
       {"i" , {{'d'}}},
       {"j" , {{'t'}}},
       {"k" , {{'s'}}},
       {"l" , {{'r'}}},
       {"m" , {{'q'}}},
       {"n" , {{"apostrophe"}}}, // '
       {"o" , {{'l'}}},
       {"p" , {{'j'}}},
       {"q" , {{'b'}}},
       {"r" , {{'o'}}},
       {"s" , {{'u'}}},
       {"t" , {{"egrave"}}}, // è
       {"u" , {{'v'}}},
       {"v" , {{"period"}}}, // .
       {"w" , {{"eacute"}}}, // é
       {"x" , {{'y'}}},
       {"y" , {{"agrave"}}}, // à
       {"z" , {{"dead_circumflex"}}}, // ^
       {"keyboard", {{"section"}, {"dollar"}}}, // §$
       {"keyboardCH", {{"c"}, {"x"}, {"h"}}},
       {"keyboardBEPO", {{"b"}, {"k"}, {"q"}}}
      },
      {{"eDP-1" , "internal", Key('i')}, // screen
       {"VGA-1" , "VGA",      Key('v')},
       {"DP-1"  , "DP1",      Key('d')},
       {"HDMI-1", "HDMI1",    Key('h')},
       {"DP-2"  , "DP2",      Key('e')},
       {"HDMI-2", "HDMI2",    Key('z')},
       {"DP-2-1", "DP21",     Key('x')},
       {"DP-2-2", "DP22",     Key('c')}}
      );

  cout << modes;

  return EXIT_SUCCESS;
}

