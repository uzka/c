
#include <cstdlib>
#include <iostream>

#include "confgenlib.h"

using namespace std;


int main() {

  map<string, Mode> modes;
  modes = confgen(
      {{"left"  , {{"j"     }, {"left"}}}, // key
       {"down"  , {{"k"     }, {"down"}}},
       {"up"    , {{"l"     }, {"up"}}},
       {"right" , {{"eacute"}, {"right"}}},
       {"a" , {{'a'}}},
       {"b" , {{'b'}}},
       {"c" , {{'c'}}},
       {"d" , {{'d'}}},
       {"e" , {{'e'}}},
       {"f" , {{'f'}}},
       {"g" , {{'g'}}},
       {"h" , {{'h'}}},
       {"i" , {{'i'}}},
       {"j" , {{'j'}}},
       {"k" , {{'k'}}},
       {"l" , {{'l'}}},
       {"m" , {{'m'}}},
       {"n" , {{'n'}}},
       {"o" , {{'o'}}},
       {"p" , {{'p'}}},
       {"q" , {{'q'}}},
       {"r" , {{'r'}}},
       {"s" , {{'s'}}},
       {"t" , {{'t'}}},
       {"u" , {{'u'}}},
       {"v" , {{'v'}}},
       {"w" , {{'w'}}},
       {"x" , {{'x'}}},
       {"y" , {{'y'}}},
       {"z" , {{'z'}}},
       {"keyboard", {{"section"}, {"dollar"}}}, // §$
       {"keyboardCH", {{"c"}, {"x"}, {"h"}}},
       {"keyboardBEPO", {{"b"}, {"k"}, {"q"}}}
      },
      {{"eDP-1" , "internal", Key('i')}, // screen
       {"VGA-1" , "VGA",      Key('v')},
       {"DP-1"  , "DP1",      Key('d')},
       {"HDMI-1", "HDMI1",    Key('h')},
       {"DP-2"  , "DP2",      Key('e')},
       {"HDMI-2", "HDMI2",    Key('z')},
       {"DP-2-1", "DP21",     Key('x')},
       {"DP-2-2", "DP22",     Key('c')}}
      );

  cout << modes;


  return EXIT_SUCCESS;
}

