#include "confgenlib.h"
#include "color.h"

using namespace std;

Mode::Mode(string name) 
  : name(name) 
{ 
}
string Key::getTouch() const { return touch; }

Key::Key(string touch, int modifier) 
  : touch(touch), modifier(modifier) 
{
}
Key::Key(char touch, int modifier) 
  : Key(string(1, touch), modifier)
{
}

Key& Key::operator += (int rhs) {
  this->modifier |= rhs;
  return *this;
}
Key& Key::operator -= (int rhs)
{
  modifier &= ~rhs;
  return *this;
}
vector<Key> operator + (vector<Key> lhs, int rhs) {
  for(Key& key : lhs)
    key += rhs;
  return lhs;
}
vector<Key> operator - (vector<Key> lhs, int rhs) {
  for(Key& key : lhs)
    key -= rhs;
  return lhs;
}
Key operator + (Key lhs, int rhs) {
  lhs += rhs;
  return lhs;
}
Key operator - (Key lhs, int rhs) {
  lhs -= rhs;
  return lhs;
}
bool operator < (const Key& lhs, const Key& rhs) {
  if(lhs.touch != rhs.touch) return lhs.touch < rhs.touch;
  return lhs.modifier < rhs.modifier;
}
void Mode::bind(const Key& key, const Action& action) {
  actions.insert({key, {action}});
}
void Mode::bind(const vector<Key>& key, const Action& action) {
  for(const Key i : key)
    actions.insert({i, {action}});
}
void Mode::bind(const Key& key, const vector<Action>& action) {
  actions.insert({key, action});
}
void Mode::bind(const vector<Key>& key, const vector<Action>& action) {
  for(const Key i : key)
    actions.insert({i, action});
}
Action::Action(string command) 
  : command(command) 
{
}
ostream& operator << (ostream& os, const Mode& rhs) {
  string prefix = "";
  if(rhs.name != "default") {
    prefix = "\t";
    os << "mode \"" << rhs.name << "\" {" << endl;
  } else {
    os << "floating_modifier $mod" << endl;
  }


  for(const pair<string, string>& i : rhs.variables){
    if(i.second.find(" ") == string::npos) {
      os << prefix << "set $" << i.first << " " << i.second << endl;
    } else {
      os << prefix << "set $" << i.first << " \"" << i.second << "\"" << endl;
    }
  }

  for(const pair<Key, vector<Action>>& i : rhs.actions) {
    os << prefix << "bindsym " << i.first << " " << i.second << endl;
  }

  for(const pair<string, Workspace>& i : rhs.assigned) {
    os << "assign [" << i.first << "] $ws" << i.second.getId() << endl;
  }
  for(const pair<string, vector<Action>>& i : rhs._for_window) {
    os << "for_window [" << i.first << "] " << i.second << endl;
  }
  for(const string& i : rhs._no_focus) {
    os << "no_focus [" << i << "] " << endl;
  }

  if(rhs.name != "default") {
    os << prefix << "bindsym Escape mode \"default\"" << endl;
    os << "}" << endl;
  } else {
    // BRUT TODO
    os << "bar {" << endl;
    os << "\tstatus_command i3status" << endl;
    os << "\tcolors {" << endl;
    //os << "\t\tstatusline #FFFF00 " << endl;

    os << "\t\tbackground #" << Color().darkness().darkness() << endl;
    os << "\t\tstatusline #" << Color().lightness() << endl;
    os << "\t\tseparator  #" << Color().lightness() << endl;
    os << "\t}" << endl;
    os << "\tfont pango:DejaVu Sans Mono 9" << endl;
    os << "\tposition top" << endl;
    os << "}" << endl;

    os << Action::exec("synclient NaturalScrolling=1 VertScrollDelta=-113");
  }
  return os;

}
ostream& operator << (ostream& os, const vector<Action>& rhs) {
  for(auto it = rhs.begin(); it != rhs.end(); ++it) {
    if(it != rhs.begin()) os << ", ";
    os << *it;
  }
  return os;
}
ostream& operator << (ostream& os, const Action& rhs) {
  return os << rhs.command;
}
ostream& operator << (ostream& os, const Key& rhs) {
  if(rhs.modifier & Key::M) os << "$mod+";
  if(rhs.modifier & Key::S) os << "Shift+";
  os << rhs.touch;
  return os;
}
Action Action::exec(string command, bool silence) {
  if(silence)
    return Action("exec --no-startup-id \""+command+"\"");
  else
    return Action("exec \""+command+"\"");
}
Action Action::i3(string command) {
  return Action(command);
}
void Mode::setVariable(string key, string value) {
  variables.insert({key, value});
}
Workspace::Workspace(const string& id, const string& name, const Key& key, bool direct) 
  : id(id), name(name)
{
  this->key.insert({key, direct});
}
Workspace::Workspace(const string& id, const string& name, bool direct)
  : Workspace(id, name, Key(id), direct)
{
}
vector<Key> Workspace::getKeysDirect() const {
  vector<Key> v;
  for(const pair<Key, bool>& i : key) {
    if(i.second) v.push_back(i.first);
  }
  return v;
}
vector<Key> Workspace::getKeys() const {
  vector<Key> v;
  for(const pair<Key, bool>& i : key) {
    v.push_back(i.first);
  }
  return v;
}
string Workspace::getId() const {
  return id; 
}
string Workspace::getName() const { 
  return name; 
}
Action Action::mode(const Mode& mode) {
  return Action("mode \""+mode.getName()+"\"");
}
Action::Action(const Mode& mode) 
  : Action("mode \""+mode.getName()+"\"")
{
}
Action Action::ask(const string& message, const string& btn, const string& action) {
  return Action::exec("i3-nagbar -t warning -m '"+message+"' -b '"+btn+"' '"+action+"'");
}
string Mode::getName() const {
  return name;
}
Screen::Screen(const string& id, const string& name, const Key& key) 
  : key({key}), id(id), name(name)
{
}
Screen::Screen(const string& id, const Key& key)
  : Screen(id, id, key)
{
}
string Screen::getId() const { return id; }
string Screen::getName() const { return name; }
vector<Key> Screen::getKeys() const { return key; }
ostream& operator << (ostream& os, const map<string, Mode>& rhs) {
  for(const pair<string, Mode> i : rhs) {
    os << "# " << i.first << endl;
    os << i.second << endl;
  }
  return os;
}
void Mode::assign(const string& match, const Workspace& ws) {
  assigned.insert({match, ws});
}
void Mode::for_window(const string& match, const vector<Action>& action) {
  _for_window.insert({match, action});
}
void Mode::for_window(const string& match, const Action& action) {
  _for_window.insert({match, {action}});
}
void Mode::no_focus(const std::string& match) {
	_no_focus.push_back(match);
}

std::vector<Action>& Mode::operator [] (const Key& k) { return actions[k]; }
bool Mode::exists(const Key& k) { return actions.count(k); }

map<string, Mode> confgen(
    map<string, vector<Key>> key,
    vector<Screen> screen
) {

  map<string, Mode> modes;

  vector<Workspace> ws;

  for(int i = 1; i <= 10; ++i) 
    ws.push_back({to_string(i), to_string(i), Key(to_string(i%10)), true});

  Workspace wsheig("h", "HEIG");
  Workspace wscomm("c", "Comm");

  ws.push_back(wsheig);
  ws.push_back(wscomm);

  modes.insert({"def", {}});
  modes["def"].setVariable("mod", "Mod4");

  modes["def"].bind(Key("Return", Key::M)        , Action::exec("i3-sensible-terminal", false));
  modes["def"].bind(key["q"] + Key::MS           , Action::i3("kill"));
  modes["def"].bind(key["d"] + Key::MS           , Action::exec("dmenu_run"));
  modes["def"].bind(key["d"] + Key::M, Action::exec("rofi -combi-modi 'window,drun,run' -modi 'window,drun,run,combi' -show combi"));// -show-icons -lines 26 -width 80 -sidebar-mode")); // -filter ^ -matching regex"));

  modes["def"].bind(Key("XF86MonBrightnessDown") , Action::exec("setbacklight -f -50"));
  modes["def"].bind(Key("XF86MonBrightnessUp")   , Action::exec("setbacklight -f +50"));
  modes["def"].bind(Key("XF86AudioMute"               ) , Action::exec("pactl set-sink-mute @DEFAULT_SINK@ toggle"));
  modes["def"].bind(Key("XF86AudioLowerVolume"        ) , Action::exec("pactl set-sink-volume @DEFAULT_SINK@  -3db"));
  modes["def"].bind(Key("XF86AudioRaiseVolume"        ) , Action::exec("pactl set-sink-volume @DEFAULT_SINK@  +3db"));
  modes["def"].bind(Key("XF86AudioLowerVolume", Key::S) , Action::exec("pactl set-sink-volume @DEFAULT_SINK@ -10db"));
  modes["def"].bind(Key("XF86AudioRaiseVolume", Key::S) , Action::exec("pactl set-sink-volume @DEFAULT_SINK@ +10db"));

  {
    map<string, pair<string, string>> lst = {
      {"F1", {"XF86AudioMute", "Mute"}},
      {"F2", {"XF86AudioLowerVolume", "Vol-"}},
      {"F3", {"XF86AudioRaiseVolume", "Vol+"}},
      {"F5", {"XF86MonBrightnessDown", "Lum-"}},
      {"F6", {"XF86MonBrightnessUp", "Lum+"}}
    };
    std::string name = "XF86 ";
    std::string namelock = "XF86 LOCK ";
    for(auto i : lst) {
      name += i.first + " " + i.second.second + "; ";
      namelock += i.first + " " + i.second.second + "; ";
    }
    modes.insert({"xf86", name});
    modes.insert({"xf86lk", namelock});
    modes["def"].bind(key["x"] + Key::M , Action::mode(modes["xf86"]));
    modes["def"].bind(key["x"] + Key::MS, Action::mode(modes["xf86lk"]));
    for(auto i : lst) {
      for(auto m : {0, Key::M, Key::S, Key::MS}) {
        if(modes["def"].exists(Key(i.second.first, m))) {
          std::vector<Action> tmp(modes["def"][Key(i.second.first, m)]);
          modes["xf86lk"].bind(Key(i.first, m), tmp);
          tmp.push_back(Action::mode(modes["def"]));
          modes["xf86"].bind(Key(i.first, m), tmp);
        }
      }
    }
  }

  for(const string& i : vector<string>({"left", "down", "up", "right"})) {
    modes["def"].bind(key[i]  + Key::M        , Action::i3("focus "+i));
    modes["def"].bind(key[i]  + Key::MS       , Action::i3("move "+i));
  }
  modes["def"].bind(key["v"] + Key::M            , Action::i3("split v"));
  modes["def"].bind(key["v"] + Key::MS           , Action::i3("split h"));
  modes["def"].bind(key["f"] + Key::M            , Action::i3("fullscreen"));
  modes["def"].bind(key["s"] + Key::M            , Action::i3("layout stacking"));
  modes["def"].bind(key["w"] + Key::M            , Action::i3("layout tabbed"));
  modes["def"].bind(key["e"] + Key::M            , Action::i3("layout toggle split"));
  modes["def"].bind(Key("space", Key::MS)        , Action::i3("floating toggle"));
  modes["def"].bind(Key("space", Key::M)         , Action::i3("focus mode_toggle"));
  modes["def"].bind(key["a"] + Key::M            , Action::i3("focus parent"));
  modes["def"].bind(key["a"] + Key::MS           , Action::i3("focus child"));
  modes["def"].bind(key["c"] + Key::MS           , Action::i3("reload"));
  modes["def"].bind(key["r"] + Key::MS           , Action::i3("restart"));

  for(const Workspace& ws : ws) {
    modes["def"].setVariable("ws"+ws.getId(), ws.getName());
    modes["def"].bind(ws.getKeysDirect() + Key::M, {Action::i3("workspace $ws" + ws.getId()), Action::i3("scratchpad show")});
    modes["def"].bind(ws.getKeysDirect() + Key::MS, Action::i3("move container to workspace $ws" + ws.getId()));
  }

  modes["def"].bind(Key("F12", Key::M ) , Action::exec("pass -c $(cd ~/.password-store/; find -type f -not -path '*/\\.*' | grep './' | sed 's~^./~~;s~\\.gpg$~~' | rofi -dmenu)"));
  modes["def"].bind(Key("F12", Key::MS) , Action::exec("showPass"));
  modes["def"].bind(Key("F11", Key::M ) , Action::exec("showBookmark"));


  modes.insert({"ws", {"workspace [0-9] h->HEIG c->Comm"}});
  modes["def"].bind(Key("Tab", Key::M), Action::mode(modes["ws"]));
  for(const Workspace& ws : ws) {
    modes["ws"].bind(ws.getKeys(),           {Action::i3("workspace $ws" + ws.getId()), Action::i3("scratchpad show")});
    modes["ws"].bind(ws.getKeys() + Key::S,   Action::i3("move container to workspace $ws" + ws.getId()));
    modes["ws"].bind(ws.getKeys() + Key::M,  {Action::i3("workspace $ws" + ws.getId()), {modes["def"]}, Action::i3("scratchpad show")});
    modes["ws"].bind(ws.getKeys() + Key::MS, {Action::i3("move container to workspace $ws" + ws.getId()), {modes["def"]}});
  }
  
  modes.insert({"resize", {"resize"}});
  modes["def"].bind(key["r"] + Key::M, Action::mode(modes["resize"]));

  for(const auto& i : map<string, string>({{"left", "shrink width"}, {"down", "grow height"}, {"up", "shrink height"}, {"right", "grow width"}})) {
    modes["resize"].bind(key[i.first]          , Action::i3("resize "+i.second+" 10 px or 10 ppt"));
    modes["resize"].bind(key[i.first] + Key::S , Action::i3("resize "+i.second+" 2 px or 2 ppt"));
  }

  modes.insert({"sys", {"sys lock (l); quit i3 (q); poweroff (o); reboot (r); suspend (s); hibernate (h); switch user (tab)"}});
  Action lock = Action::exec(string("i3lock -c ") + Color().darkness().darkness());
  modes["def"].bind(key["m"] + Key::M , Action::mode(modes["sys"]));
  modes["sys"].bind(key["l"]          , {lock, {modes["def"]}});
  modes["sys"].bind(key["q"]          , {Action::exec("i3-msg exit"), {modes["def"]}});
  modes["sys"].bind(key["o"]          , {Action::exec("i3-nagbar -t warning -m 'poweroff' -b 'Yes, poweroff' 'systemctl poweroff'"), {modes["def"]}});
  modes["sys"].bind(key["o"]          , {Action::ask("poweroff", "Yes, poweroff", "systemctl poweroff"), {modes["def"]}});
  modes["sys"].bind(key["o"] + Key::S , {Action::exec("systemctl poweroff"), {modes["def"]}});
  modes["sys"].bind(key["r"]          , {Action::ask("reboot", "Yes, reboot", "systemctl reboot"), {modes["def"]}});
  modes["sys"].bind(key["r"] + Key::S , {Action::exec("systemctl reboot"), {modes["def"]}});
  modes["sys"].bind(key["s"]          , {lock, Action::exec("systemctl suspend"), {modes["def"]}});
  modes["sys"].bind(key["h"]          , {lock, Action::exec("systemctl hibernate"), {modes["def"]}});
  modes["sys"].bind(Key("Tab"), {lock, Action::exec("dm-tool switch-to-greeter"), {modes["def"]}});

  string screenname = "screen ";
  for(const Screen& i : screen) {
    screenname += i.getName() + "(";
    for(const Key& j : i.getKeys()) {
      screenname += j.getTouch();
    }
    screenname += ") ";
  }
  modes.insert({"screen", {screenname}});
  Action screenShowLow = Action::exec("screenShowLow");
  modes["def"].bind(key["p"] + Key::M, {{modes["screen"]}, Action::exec("screenShow")});
  for(const Screen& i : screen) {
    modes["screen"].bind(i.getKeys()           , {Action::exec("xrandr --output "+i.getId()+" --auto --right-of $(rightScreen)"), screenShowLow});
    modes["screen"].bind(i.getKeys() + Key::M  , {Action::exec("xrandr --output "+i.getId()+" --auto --left-of $(leftScreen)"), screenShowLow});
    modes["screen"].bind(i.getKeys() + Key::S  , {Action::exec("xrandr --output "+i.getId()+" --off"), screenShowLow});
    modes["screen"].bind(i.getKeys() + Key::MS , {Action::exec("xrandr --output "+i.getId()+" --auto"), screenShowLow});
  }

  string modekbname = "keyboard fr_CH("+key["keyboardCH"].front().getTouch()+") bépo("+key["keyboardBEPO"].front().getTouch()+")";
  modes.insert({"kb", {modekbname}});
  modes["def"].bind(key["keyboard"] + Key::M , Action::mode(modes["kb"]));
  modes["kb"].bind(key["keyboardCH"], {Action::exec("chkb ch_fr"), modes["def"], Action::i3("reload")});
  modes["kb"].bind(key["keyboardBEPO"], {Action::exec("chkb fr_bepo"), modes["def"], Action::i3("reload")});


  modes["def"].assign("class='Telegram'", wscomm);
  modes["def"].assign("class='Evolution'", wscomm);
  modes["def"].for_window("title=\"(?i)/home/[a-z0-9]+/bin/Bepo-1.1-complet.png$\"", {Action::i3("floating enable"), Action::i3("move position 1100 750"), Action::i3("border none")});
  modes["def"].no_focus("title=\"(?i)/home/[a-z0-9]+/bin/Bepo-1.1-complet.png$\"");
  //modes["def"].for_window("class=\"feh\"", Action::i3("floating enable"));

  return modes;
}
