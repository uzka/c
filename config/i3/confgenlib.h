#ifndef CONFGENLIB_H
#define CONFGENLIB_H

#include <ostream>
#include <string>
#include <map>
#include <vector>

class Mode;
class Key;
class Action;
class Workspace;


class Mode {
  friend std::ostream& operator << (std::ostream& os, const Mode& rhs);
  private:
    std::string name;
    std::map<Key, std::vector<Action>> actions;
    std::map<std::string, std::string> variables;
    std::map<std::string, Workspace> assigned;
    std::map<std::string, std::vector<Action>> _for_window;
    std::vector<std::string> _no_focus;
  public:
    Mode(std::string name="default");
    void bind(const Key& key, const Action& action);
    void bind(const std::vector<Key>& key, const Action& action);
    void bind(const Key& key, const std::vector<Action>& action);
    void bind(const std::vector<Key>& key, const std::vector<Action>& action);
    void assign(const std::string& match, const Workspace& ws);
    void for_window(const std::string& match, const Action& action);
    void for_window(const std::string& match, const std::vector<Action>& action);
    void no_focus(const std::string& match);
    void setVariable(std::string key, std::string value);
    std::string getName() const;
    std::vector<Action>& operator [] (const Key& k);
    bool exists(const Key& k);
    
};

std::ostream& operator << (std::ostream& os, const Mode& rhs);
std::ostream& operator << (std::ostream& os, const std::map<std::string, Mode>& rhs);

class Key {
  friend bool operator < (const Key& lhs, const Key& rhs);
  friend std::ostream& operator << (std::ostream& os, const Key& rhs);
  private:
    std::string touch;
    int modifier;
  public:
    static const int M  = 0b00001;
    static const int S  = 0b00010;
    static const int MS = Key::M | Key::S;
    Key(std::string touch, int modifier=0);
    Key(char touch, int modifier=0);
    Key& operator += (int rhs);
    Key& operator -= (int rhs);
    std::string getTouch() const;
};

Key         operator +  (const Key& lhs, int rhs);
std::vector<Key> operator +  (std::vector<Key> lhs, int rhs);
Key         operator -  (const Key& lhs, int rhs);
std::vector<Key> operator -  (std::vector<Key> lhs, int rhs);
bool        operator <  (const Key& lhs, const Key& rhs);
std::ostream&    operator << (std::ostream& os, const Key& rhs);

class Action {
  friend std::ostream& operator << (std::ostream& os, const Action& rhs);
  private:
    std::string command;
    Action(std::string command);
  public:
    Action(const Mode& mode);
    static Action exec(std::string command, bool silence=true);
    static Action i3(std::string command);
    static Action mode(const Mode& mode);
    static Action ask(const std::string& message, const std::string& btn, const std::string& action);

};

std::ostream& operator << (std::ostream& os, const std::vector<Action>& rhs);
std::ostream& operator << (std::ostream& os, const Action& rhs);

class Workspace {
  private:
    std::map<Key, bool> key;
    std::string id;
    std::string name;
  public:
    Workspace(const std::string& id, const std::string& name, const Key& key, bool direct=false);
    Workspace(const std::string& id, const std::string& name, bool direct=false);
    std::vector<Key> getKeysDirect() const;
    std::vector<Key> getKeys() const;
    std::string getId() const;
    std::string getName() const;
};
class Screen {
  private:
    std::vector<Key> key;
    std::string id;
    std::string name;
  public:
    Screen(const std::string& id, const std::string& name, const Key& key);
    Screen(const std::string& id, const Key& key);
    std::string getId() const;
    std::string getName() const;
    std::vector<Key> getKeys() const;
};

std::map<std::string, Mode> confgen(
    std::map<std::string, std::vector<Key>> key,
    std::vector<Screen> screen
);

#endif // CONFGENLIB_H
