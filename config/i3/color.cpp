#include "color.h"

#include <iomanip>
#include <sstream>

Color::Color(uint8_t r, uint8_t g, uint8_t b)
  : r(r), g(g), b(b)
{
}

Color Color::darkness() const
{
  return Color(r / 2, g / 2, b / 2);
}
Color Color::lightness() const
{
  return Color(
      (uint8_t) (255 - (255 - r) / 2), 
      (uint8_t) (255 - (255 - g) / 2), 
      (uint8_t) (255 - (255 - b) / 2)
      );
}
std::ostream& operator << (std::ostream& os, const Color& col) {
  os << std::setfill('0') << std::hex;
  os << std::setw(2) << (int) col.r;
  os << std::setw(2) << (int) col.g;
  os << std::setw(2) << (int) col.b;
  os << std::dec << std::setfill(' ');
  return os;
}
std::string& operator + (std::string& lhs, const Color& col) {
  std::stringstream ss; 
  ss << col;
  lhs += ss.str();
  return lhs;
}
std::string operator + (std::string lhs, const Color& col) {
  std::stringstream ss; 
  ss << col;
  lhs += ss.str();
  return lhs;
}
