#ifndef COLOR_H
#define COLOR_H

#include "colordefine.h"

#include <cinttypes>
#include <ostream>
#include <string>

class Color {
  friend std::ostream& operator << (std::ostream& os, const Color& col);
  friend std::string& operator + (std::string& lhs, const Color& col);
  friend std::string operator + (std::string lhs, const Color& col);
  private:
    uint8_t r;
    uint8_t g;
    uint8_t b;
  public:
    Color(uint8_t r=USERCOLOR_R, uint8_t g=USERCOLOR_G, uint8_t b=USERCOLOR_B);
    Color darkness() const;
    Color lightness() const;
};

#endif // COLOR_H

