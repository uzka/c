
" 16M de couleur
set termguicolors
" Affichage des numéros de ligne en fonction de la ligne courrente
set number relativenumber
" nombre de ligne min avant et après le curseur
set scrolloff=2
" pour <C-A> et <C-X>
"set nrformats=bin,hex
" :q ou :e avec erreur → demande confirmation
"set confirm

" TABULATION
set tabstop=2
set softtabstop=0 
set expandtab
set shiftwidth=2
set smarttab
set colorcolumn=81

" Color
"set background=dark
"set background=light
set cursorline
set cursorcolumn
colorscheme desert
"colorscheme morning
"colorscheme murphy
"colorscheme peachpuff
"colorscheme zellner

" Indentation
set cindent

" Mouse
set mouse=a

" Map terminal normal mode
tnoremap <Esc> <C-\><C-n>

set autowrite
"nmap! <F5> :make automk-%<RETURN>
nmap <F5> :wa \| make<RETURN>
imap <F5> <C-O>:wa \| make<CR>
"autocmd FileType c
"   \ set makeprg=gcc\ -Wall\ %\ -o\ output |
"   \ nnoremap  <special> <buffer> <F5> :w!<cr>:make<cr>:!./output

"################################################################################
"################################################################################
" VIM Plugin from  https://github.com/junegunn/vim-plug
call plug#begin('~/.local/share/nvim/plugged')

"####################################
" Markdown 
"####################################

" Markdown auto viewer
"https://github.com/iamcco/markdown-preview.nvim
"
" Markdown editor
"https://github.com/SidOfc/mkdx

"####################################
" LaTeX
"####################################

" [1] LaTeX auto viewer
"https://github.com/donRaphaco/neotex
Plug 'donRaphaco/neotex', { 'for': 'tex' }

"####################################
" C++
"####################################


" [3] GDB
"https://github.com/sakhnik/nvim-gdb
"Plug 'sakhnik/nvim-gdb'
"
" [4] clang_complete
" https://github.com/Rip-Rip/clang_complete
Plug 'Rip-Rip/clang_complete'

"####################################
" Git
"####################################

" Git
" [2] Git Show diff
"https://github.com/airblade/vim-gitgutter
Plug 'airblade/vim-gitgutter'
" Git command
"https://github.com/tpope/vim-fugitive
Plug 'tpope/vim-fugitive'

call plug#end()
"################################################################################
"################################################################################


" [1] LaTeX auto viewer [NeoTex]
let g:neotex_latexdiff=1

" [2] Git information 
"let g:gitgutter_highlight_lines = 1

" [3] GDB
"let g:loaded_nvimgdb = 1

" [4] clang_complete
"let g:clang_library_path='/usr/lib/llvm-7/lib/'
let g:clang_library_path='/usr/lib/llvm-7/lib/libclang.so.1'
"let g:clang_user_options='|| exit 0'
"let g:clang_complete_auto = 1
"let g:clang_complete_copen = 1
"let g:clang_auto_select=0
let g:clang_snippets=1
"let g:clang_snippets_engine = 'clang_complete'
let g:clang_complete_optional_args_in_snippets=1

imap <C-A> \


if !empty(system("setxkbmap -print|grep bepo"))
  source ~/.config/nvim/init_bepo.vim
else
  noremap é l
  noremap l k
  noremap k j
  noremap j h

  noremap É L
  noremap L K
  noremap K J
  noremap J H

  noremap gl gk
  noremap gk gj
  noremap <C-w>é <C-w>l
  noremap <C-w>l <C-w>k
  noremap <C-w>k <C-w>j
  noremap <C-w>j <C-w>h
endif
