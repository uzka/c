<?php

$XCompose = file('.XCompose.src');

function str_pop_back(string &$value, int $size=2) : string {
  $ret = substr($value, -$size);
  $value = substr($value, 0, -$size);
  return $ret;
}

foreach($XCompose as $line)
{
  if(preg_match('~^(?<def>(<[^>]+> )+): U(?<unicode>[0-9A-F]+)$~', $line, $matches)) {
    echo $matches['def'], ': "', mb_chr(hexdec($matches['unicode'])), '" ', "\n";#, $matches['unicode'], "\n";
  } else {
    echo $line;
  }
}


