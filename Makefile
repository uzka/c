
INSTDIR=~
INSTALL=cp -u

USERGROUP=$$(id -u):$$(id -g)
PHP=docker run --rm -v "$$PWD":/project -w /project -u $(USERGROUP) php php

# Compile all
all: all-config

all-config:
	cd config && $(MAKE)

#pdf: 
#dvi:
#ps:

# make archive for dist
#dist: pdf
#	mkdir test-1.0
#	cp $^ test-1.0
#	tar test-1.0

# Install prog
install:
	cd dotfile/ && $(PHP) .XCompose.php > .XCompose
	$(INSTALL) -r dotfile/. $(INSTDIR)
	cd config && $(MAKE) install INSTDIR="$(INSTDIR)/.config"
	cd bin && $(MAKE) install INSTDIR="$(INSTDIR)/bin"

# Install doc
#install-{html,dvi,pdf,ps}:
# Uninstall prog
#uninstall:

# Test prog
#check:
# Test after install (no deps to install)
#installcheck:

clean:
	rm -f dotfile/.XCompose
	cd config && $(MAKE) clean
distclean: clean
mostlyclean: clean
maintainer-clean: clean

